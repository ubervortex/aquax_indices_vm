import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
import os
from PIL import Image
#import matplotlib.pyplot as plt
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
import time 
import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta, date
import subprocess
from scipy.signal import medfilt


#-----------------------------------------------------------------
# CODE TO MAKE THE IMAGES OVERLAPPNG
#-----------------------------------------------------------------


def SST_area_cropping(f,  array_name):
    #Computation of algal bloom anomalies

    command = ['gdalbuildvrt', '-separate', '-input_file_list', 'image_name.txt', 'output_file.vrt']
    subprocess.call(command)
    
    dataset = gdal.Open ( "output_file.vrt" )
    data = dataset.ReadAsArray()
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    # 'Get georeference information'
    geotransform = dataset.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    print(len(data))
    for i in range(0, len(data)):
        name = array_name[i]
        a = data[i]
        a[a==0] = np.NaN
        #create name of the destination file
        dest_file = f  +'anomaly/' + name
        dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
        dst_ds.SetGeoTransform(geotransform)    # specify coords
        srs = osr.SpatialReference()            # establish encoding
        srs.ImportFromEPSG(4326)                # WGS84 lat/long
        dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
        dst_ds.GetRasterBand(1).WriteArray(a)   # write r-band to the raster
        dst_ds.FlushCache()                     # write to disk
        dst_ds = None
        




if __name__ == "__main__":
    # How to run the code:
    # python extract_data_
    f = '/application/pi/Documents/data_extracted/Florida/SLSTR_multi/'
    file_txt = open("image_name.txt", "w")
    array_name = []
    for root, dirs, files in os.walk(f, topdown=False):
        for name in files:
            array_name.append(name)
            file_txt.write(f + name + "\n" )      

    file_txt.close()            
    SST_area_cropping(f, array_name)