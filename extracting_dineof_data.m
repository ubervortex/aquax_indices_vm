function e = extracting_dineof_data(fold,  f)

folder = dir(fold)

out =  gread('/application/pi/Downloads/dineof-3.0/Example_CHl/Output/output.filled');
for k=1:size(out,3)
    name = folder(k+2).name;
    S = name(1:end-4);
    d = S(end-(size(S,2)-19):end);
    
    [row, col] = find(out(:,:,k)==out(4,1,k));
    a = out(:,:,k);
    for i =1:size(row,1)
        a(row(i,1),col(i,1)) = NaN;
    end
    
    d = strrep(d,'_','');
    
    file_name = strcat(f, 'TSM_din_');
    file_name = strcat(file_name, d);
    file_name = strcat(file_name,'.tif');
%     imwrite(out(:,:,k), file_name, 'BitDepth',16)
%     info = imfinfo(filename);
    filen_name = strcat(f, file_name);
    t = Tiff(file_name, 'w'); 
    tagstruct.ImageLength = size(out(:,:,k), 1); 
    tagstruct.ImageWidth = size(out(:,:,k), 2); 
    tagstruct.Compression = Tiff.Compression.None; 
    tagstruct.SampleFormat = Tiff.SampleFormat.IEEEFP; 
    tagstruct.Photometric = Tiff.Photometric.MinIsBlack; 
	tagstruct.BitsPerSample = 32; % 32; 
    tagstruct.SamplesPerPixel = 1; % 1; 
    tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; 
    t.setTag(tagstruct); 
    t.write(single(a)); 
    t.close();
end

e = 1;