import subprocess
import cStringIO,io
import os
import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
from envi_to_np import extract_geotiff
from index_computation import cyanobacteria_index, CHL_index
from area_cropping import area_cropping
from xml.etree import ElementTree as et

#-----------------------------------------------------------------
# CODE TO EXTRACT THE IMAGES OF INTEREST FROM THE RAW DATA  
# CHECK THE CORRECT SNAP GRAPH
#-----------------------------------------------------------------


snap_gpt = "gpt"


def extract_aquisition_date(file_name):
    #Function to extract the aquisition date
    

    tree = ET.parse(file_name + '/xfdumanifest.xml')
    root = tree.getroot()
    
    #cycle to extract the acquisition data
    for attributes in root.iter('metadataSection'):
        attribute_list = attributes.getchildren()
        for attribute in attribute_list:
            attribute_list_meta =  attribute.getchildren()
            for att in attribute_list_meta:
                xmlData= att.getchildren()
                for aaaa in xmlData:
                    for data in aaaa.iter('{http://www.esa.int/safe/sentinel/1.1}acquisitionPeriod'):
                        acquisition_data = data[0].text
    
    acquisition_data =  acquisition_data[0:10]
    acquisition_data =  acquisition_data.replace('-','_')

    return acquisition_data


def folder_creation(file_name, region, sensor):

    #Function to setup the folder where to store the elaborated images
    acquisition_data = extract_aquisition_date(file_name)

    f = '/application/pi/Documents/data_extracted/' + region + '/' + sensor + '/'
    # # check if the folder exist or not
    if not os.path.exists(f):
    #     #folder not present -> create it
        os.makedirs(f)

    return f, acquisition_data


def organize_file(f,acquisition_data):
    
    print ('ORGANIZER')
    cwd = os.getcwd() # current folder
    file_path = cwd + '/target.data/'

    #cyanobacteria bands
    #'Oa08_reflectance.hdr','Oa10_reflectance.hdr','Oa11_reflectance.hdr','Oa08_reflectance.img','Oa10_reflectance.img','Oa11_reflectance.img','Oa08_reflectance.img','Oa10_reflectance.img','Oa11_reflectance.img','Oa08_reflectance.hdr','Oa10_reflectance.hdr','Oa11_reflectance.hdr'


    #s3_l1
    # possible_name = ['Oa01_radiance.nc','Oa02_radiance.nc','Oa03_radiance.nc','Oa04_radiance.nc','Oa05_radiance.nc','Oa06_radiance.nc',
    # 'Oa07_radiance.nc','Oa08_radiance.nc','Oa09_radiance.nc','Oa10_radiance.nc','Oa11_radiance.nc','Oa12_radiance.nc','Oa13_radiance.nc',
    # 'Oa14_radiance.nc','Oa15_radiance.nc','Oa16_radiance.nc','Oa17_radiance.nc','Oa18_radiance.nc','Oa19_radiance.nc','Oa20_radiance.nc',
    # 'Oa21_radiance.nc']
    possible_name = ['Oa03_reflectance.hdr','Oa04_reflectance.hdr','Oa05_reflectance.hdr','Oa06_reflectance.hdr','Oa03_reflectance.img',
    'Oa04_reflectance.img','Oa05_reflectance.img','Oa06_reflectance.img']

    #s3_l2
    possible_name = ['ADG443_NN.img', 'TSM_NN.img', 'CHL_OC4ME.img', 'ADG443_NN.hdr', 'TSM_NN.hdr', 'CHL_OC4ME.hdr', 
    'Oa03_reflectance.hdr','Oa04_reflectance.hdr','Oa05_reflectance.hdr','Oa06_reflectance.hdr','Oa03_reflectance.img',
    'Oa04_reflectance.img','Oa05_reflectance.img','Oa06_reflectance.img']

    for root, dirs, files in os.walk(file_path, topdown=False):
        for name in dirs:
            shutil.rmtree(file_path + name)

    for root, dirs, files in os.walk(file_path, topdown=False):
        for name in files:
            if any(name in s for s in possible_name):
                
                extract_geotiff(file_path,  name, f, acquisition_data)
            else:
                os.remove(file_path + name)
    
    for root, dirs, files in os.walk(file_path, topdown=False):
        for name in files:
            os.remove(file_path + name)



def get_metadata(file_name, region_polygon):
    
    #update the graph according to the selected region
    if os.path.isfile(file_name + '/' + 'Oa03_radiance.nc'): 
        data_level = 'L1'
        graph = "L1_graph_processing.xml" 
    
    if os.path.isfile(file_path + '/' + 'Oa03_reflectance.nc'):
        data_level = 'L2'
        graph = "L2_graph_processing.xml"

    tree = et.parse(graph)
    tree.find('.//geoRegion').text = region_polygon
    tree.write(graph)     

    # Create the command line
    try:
        cmd = [snap_gpt, graph, '-Pfile='+file_name]
        dta = subprocess.check_output(cmd, universal_newlines=True)
    except:
        pass
    
    return 'ok', data_level


# if __name__ == "__main__":
#     # How to run the code:
#     # python extract_data_snap.py '/application/pi/Documents/Sentinel-download/Italia/S3/OLCI/' Region Sensor Parameter

#     # parameter choose between: CHL, CHL_OC4, ADG443_NN_, TSM_NN_

#     #Main Code to extract all the index of interest
#     origin_folder = sys.argv[1]
#     region = sys.argv[2]
#     sensor = sys.argv[3]
#     parameter = sys.argv[4]

#     print('INITIALIZATION')
#     for root, dirs, files in os.walk(origin_folder, topdown=False):
#         for name in dirs:
#             file_path = origin_folder + name
#             ret = get_metadata(file_path)
#             if ret == 'ok':
#                 f,acquisition_data = folder_creation(file_path, region, sensor)
#                 organize_file(f, acquisition_data)

#                 #ADD HERE OTHER EVENTUAL CODE FOR OTHER INDECES

#                 #INDEX computatation
#                 # try:
#                 #     cyanobacteria_index(f, acquisition_data)
#                 # except:
#                     # pass
#                 try:
#                     CHL_index(f, acquisition_data)
#                 except:
#                     pass

#     #cerate overlapping images
#     #add a for cycle if we have more parameters
#     area_cropping(f + '/',  parameter)  

#     #create image ready for dineoff
#     # project_file(f + '/' + parameter + '/',  parameter)
                
    
    
                   



