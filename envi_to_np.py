
import os
import sys
import shutil
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr


#-----------------------------------------------------------------
	# '''
	# Converts a binary file of ENVI type to a numpy array.
	# Lack of an ENVI .hdr file will cause this to crash.
	# '''
#-----------------------------------------------------------------


def extract_geotiff(file_path,  file_name, dest_folder, acquisition_data):

	ext = file_name[-4:] 
	if ext == '.img':
		inDs = gdal.Open(file_path + file_name, GA_ReadOnly)
		
		if inDs is None:
			sys.exit("Try again!")
		else:

			# 'Get image size'
			cols = inDs.RasterXSize
			rows = inDs.RasterYSize
			bands = inDs.RasterCount
		
			# 'Get georeference information'
			geotransform = inDs.GetGeoTransform()
			originX = geotransform[0]
			originY = geotransform[3]
			pixelWidth = geotransform[1]
			pixelHeight = geotransform[5]

			# Set pixel offset.....
			# 'Convert image to 2D array'
			band = inDs.GetRasterBand(1)
			image_array = band.ReadAsArray(0, 0, cols, rows)
			image_array_name = file_name
			#image_array=  image_array*(-1)

			#create name of the destination file
			dest_file = file_name[:-4] + '_' + acquisition_data + '.tif'
			dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)

			dst_ds.SetGeoTransform(geotransform)    # specify coords
			srs = osr.SpatialReference()            # establish encoding
			srs.ImportFromEPSG(4326)                # WGS84 lat/long
			dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
			dst_ds.GetRasterBand(1).WriteArray(image_array)   # write r-band to the raster
			dst_ds.FlushCache()                     # write to disk

			dst_ds = None
			
			#move the file in the correct folder
			shutil.move(dest_file, dest_folder + dest_file) 
         