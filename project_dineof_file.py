import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
import os
from PIL import Image
#import matplotlib.pyplot as plt
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
import time 
import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta, date
import subprocess
from scipy.signal import medfilt
from numpy import array
import math


#-----------------------------------------------------------------
# CODE TO PROJET THE DINEOFF FILE
#-----------------------------------------------------------------


def project_file(f):
    #load original dataset
    dataset = gdal.Open ( "output_file.vrt" )
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    # 'Get georeference information'
    geotransform = dataset.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    for root, dirs, files in os.walk(f, topdown=False):
        for name in files:
            #create name of the destination file
            image = Image.open(f + name)
            img = array(image) 

            date =  name.replace('TSM_din_', '')
            date = date.replace('.tif', '')
            date_f = date.replace('_', '')
            dest_file = f + 'TSM_' + date_f + '.tif'

            dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
            dst_ds.SetGeoTransform(geotransform)    # specify coords
            srs = osr.SpatialReference()            # establish encoding
            srs.ImportFromEPSG(4326)                # WGS84 lat/long
            dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
            dst_ds.GetRasterBand(1).WriteArray(img)   # write r-band to the raster
            dst_ds.FlushCache()                     # write to disk
            dst_ds = None
            os.remove(f + name)
           





if __name__ == "__main__":
    f = '/application/pi/Documents/data_extracted/demo_clients/TSM/'
    # f = '/application/pi/Documents/data_extracted/Florida/OLCI/anomaly/'
    project_file(f)