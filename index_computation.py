import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
import os
from PIL import Image
#import matplotlib.pyplot as plt
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
import timestamp
import time 
import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta, date

def cyanobacteria_index(f, aquisition_data):
    #Cyanobacterya index
    # L_ = 665, L= 681, L_p = 709

    #CI = SS(681) 
    #SS(L) = L - L(-) - {L(+) - L(-)} * (L - L(-)) / (L(+) - L(-)
    file_name = f  +'/Oa08_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
    L_ = np.array(image)

    file_name = f  +'/Oa10_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
        os.remove(file_name)
    L = np.array(image)

    file_name = f  +'/Oa11_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
        os.remove(file_name)
    L_p = np.array(image)

    a = L - L_ - (L_p - L_) *0.37
    SS = -a

    file_name = f  +'/Oa08_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        inDS = gdal.Open(file_name)
        os.remove(file_name)
    
    
    cols = inDS.RasterXSize
    rows = inDS.RasterYSize
    # 'Get georeference information'
    geotransform = inDS.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    #create name of the destination file
    dest_file = f  +'/CyanoIndex_' + aquisition_data + '.tif'
    dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform(geotransform)    
    srs = osr.SpatialReference()            
    srs.ImportFromEPSG(4326)                
    dst_ds.SetProjection(srs.ExportToWkt()) 
    dst_ds.GetRasterBand(1).WriteArray(SS)   
    dst_ds.FlushCache()                     
    dst_ds = None



def CHL_index(f, aquisition_data):
    #Cyanobacterya index
    # L_ = 665, L= 681, L_p = 709

    #CI = SS(681) 
    #SS(L) = L - L(-) - {L(+) - L(-)} * (L - L(-)) / (L(+) - L(-)
    file_name = f  +'/Oa03_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
        os.remove(file_name)
    b1 = np.array(image)

    file_name = f  +'/Oa04_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
    b2 = np.array(image)

    file_name = f  +'/Oa05_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
        os.remove(file_name)
    b3 = np.array(image)

    file_name = f  +'/Oa06_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
        os.remove(file_name)
    b4 = np.array(image)
    
    
    R1 = np.maximum.reduce([b1,b2,b3])
    R2 = R1 / b4
    R = np.log10(R2)
    CHL_OC4 = np.power(10,(0.4502748 -3.259491*R + 3.522731*pow(R,2) - 3.359422*pow(R,3) - 0.949586*pow(R,4)))

    file_name = f  +'/Oa04_reflectance_' + aquisition_data + '.tif'
    if os.path.exists(file_name):
        inDS = gdal.Open(file_name)
        os.remove(file_name)
      
    
    cols = inDS.RasterXSize
    rows = inDS.RasterYSize
    # 'Get georeference information'
    geotransform = inDS.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    #create name of the destination file
    dest_file = f  +'/CHL_' + aquisition_data + '.tif'
    dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform(geotransform)    # specify coords
    srs = osr.SpatialReference()            # establish encoding
    srs.ImportFromEPSG(4326)                # WGS84 lat/long
    dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
    dst_ds.GetRasterBand(1).WriteArray(CHL_OC4)   # write r-band to the raster
    dst_ds.FlushCache()                     # write to disk
    dst_ds = None


def sum_nan_arrays(a,b):
    ma = np.isnan(a)
    mb = np.isnan(b)
    ave = np.where(ma&mb, np.nan, np.where(ma,0,a) + np.where(mb,0,b))
    return ave

def create_dataset(f, actual_date, previous_date_m):
    f = open("image_name.txt", "w")
    for single_date in daterange(previous_date_m, actual_date):
        #extract the base image
        file_name = f  +'/CHL_' + single_date + '.tif'
        if os.path.isfile(file_name) == 'False':
                file_name = f  +'/CHL_OC4' + single_date + '.tif'
        f.write(file_name + "\n" )
    f.close()
                
    cmd = ['gdalbuildvrt -separate -input_file_list image_name.txt output_file.vrt', graph, '-Pfile='+file_name]
    dta = subprocess.check_output(cmd, universal_newlines=True)   
  
    dataset = gdal.Open ( "output_file.vrt" )
    data = dataset.ReadAsArray()

    mean_im = sum_nan_arrays(data[0],data[1])

    for i in range(2, len(data)):
        mean_im = sum_nan_arrays(mean_im,data[i])

    mean_im = mean_im/len(data)
  
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    # 'Get georeference information'
    geotransform = dataset.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    #create name of the destination file
    dest_file = f  +'/CHL_anomaly' + aquisition_data + '.tif'
    dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform(geotransform)    # specify coords
    srs = osr.SpatialReference()            # establish encoding
    srs.ImportFromEPSG(4326)                # WGS84 lat/long
    dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
    dst_ds.GetRasterBand(1).WriteArray(mean_im)   # write r-band to the raster
    dst_ds.FlushCache()                     # write to disk
    dst_ds = None


def CHL_anomaly(f):
    #Computation of algal bloom anomalies
    for root, dirs, files in os.walk(origin_folder, topdown=False):
        for name in dirs:
            if 'CHL' in name:
                date = name.replace('CHL', '')
            if 'CHL_OC4' in name:
                date =  name.replace('CHL_OC4', '')


            timestamp= datetime.strptime(date,  "%Y/%m/%d")
            two_months_earlier = timestamp - relativedelta(months=2)
            create_dataset(f, timestamp, two_months_earlier)

            