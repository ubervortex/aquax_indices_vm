import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
import os
from PIL import Image
#import matplotlib.pyplot as plt
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
import time 
import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta, date
import subprocess
from scipy.signal import medfilt


#-----------------------------------------------------------------
# CODE TO MAKE THE IMAGES OVERLAPPNG
#-----------------------------------------------------------------


def map_to_pixel(mx,my,gt):
    #Convert from map to pixel coordinates.
    #Only works for geotransforms with no rotation.
    px = int((mx - gt[0]) / gt[1]) #x pixel
    py = int((my - gt[3]) / gt[5]) #y pixel

    return px,py

def extent_to_offset(xmin,ymin,xmax,ymax,gt):
    pxmin,pymin = map_to_pixel(xmin,ymin,gt)
    pxmax,pymax = map_to_pixel(xmax,ymax,gt)

    return pxmin,pymin,pxmax-pxmin,pymax-pymin


def coord2pixel(x, y, transform_prev):
    """Convert Lat/lon to pixel position"""
    xOrigin = transform_prev[0]
    yOrigin = transform_prev[3]
    pixelWidth = transform_prev[1]
    pixelHeight = -transform_prev[5]
    col = int((x - xOrigin) / pixelWidth)
    row = int((yOrigin -y ) / pixelHeight)
    return(col, row)


def area_cropping(f, param):
    #create txt file for file of interest
    file_txt = open("image_name.txt", "w")
    dates =  []
    for root, dirs, files in os.walk(f, topdown=False):
        for name in files:     
            date = ""
            if param in name:
                date =  name.replace(param, '')
                date = date.replace('.tif', '')
            if date:
                # timestamp= datetime.strptime(date,  "%Y_%m_%d")
                dates.append(date)
                file_txt.write(f + name + "\n" )

    file_txt.close()

    #create cube of images to made them overlapping
    subprocess.call('gdalbuildvrt -separate -input_file_list image_name.txt output_file.vrt', shell=True)
    dataset = gdal.Open ( "output_file.vrt" )
    data = dataset.ReadAsArray()
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    # 'Get georeference information'
    geotransform = dataset.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    #save images
    for i in range(0, len(data)):
        data[i][np.where((data[i] ==0))] = float('NaN')
        data[i] = np.power(10,data[i])

        #create name of the destination file
        dest_file = f  + param + '/' + param + '_' + str(dates[i]) + '.tif'
        dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
        dst_ds.SetGeoTransform(geotransform)    # specify coords
        srs = osr.SpatialReference()            # establish encoding
        srs.ImportFromEPSG(4326)                # WGS84 lat/long
        dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
        dst_ds.GetRasterBand(1).WriteArray(data[i])   # write r-band to the raster
        dst_ds.FlushCache()                     # write to disk
        dst_ds = None
        
# def project_file(f, param):
#     #load original dataset
#     dataset = gdal.Open ( "output_file.vrt" )
#     cols = dataset.RasterXSize
#     rows = dataset.RasterYSize
#     # 'Get georeference information'
#     geotransform = dataset.GetGeoTransform()
#     originX = geotransform[0]
#     originY = geotransform[3]
#     pixelWidth = geotransform[1]
#     pixelHeight = geotransform[5]

#     for root, dirs, files in os.walk(f, topdown=False):
#         for name in files:
#             #create name of the destination file
#             image = Image.open(f + '/'+name)
#             img = np.array(image) 
#             img = np.power(10,img)
#             #img[np.where((img < 0.001))] = float('NaN')

#             date =  name.replace(param, '')
#             date = date.replace('.tif', '')

#             dest_file = f + 'projected_' + param + '_' + date + '.tif'
#             dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
#             dst_ds.SetGeoTransform(geotransform)    # specify coords
#             srs = osr.SpatialReference()            # establish encoding
#             srs.ImportFromEPSG(4326)                # WGS84 lat/long
#             dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
#             dst_ds.GetRasterBand(1).WriteArray(img)   # write r-band to the raster
#             dst_ds.FlushCache()                     # write to disk
#             dst_ds = None
#             os.remove(f + '/'+name)
           


# if __name__ == "__main__":
#     # How to run the code:
#     # python extract_data_
#     c = 'CDM'

#     f = '/application/pi/Documents/data_extracted/Florida/OLCI/'
#     area_cropping(f)
#     f = '/application/pi/Documents/data_extracted/Florida/OLCI/TSM/'
#     project_file(f)