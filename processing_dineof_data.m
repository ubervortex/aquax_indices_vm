function c =  processing_dineof_data(folder_name)

folder = dir(folder_name);

name = folder(3).name;
S = name(1:end-4);
%initial_date = S(end-(size(S,2)-5):end);
%initial_date = [initial_date(5:6), '/', initial_date(7:8), '/', initial_date(1:4)];
initial_date = S(end-(size(S,2)-19):end);

cube = [];
count = 0;
for k=3:size(folder,1)
    name = folder(k).name;
    S = name(1:end-4);
    d = S(end-(size(S,2)-19):end);
   % d = [d(5:6), '/', d(7:8), '/', d(1:4)];
    numdays = datenum(d) - datenum(initial_date);
    count = count+1;
    file_name = strcat(folder_name,name);
    cube(:,:,count) = imread(file_name);
    cube(:,:,count) = (cube(:,:,count));  %pay attention at these line
    
    dates(1,count) = numdays+1;   
end

mask = tril(ones(size(cube,1), size(cube,2)),-1);
mask = imread('/application/pi/Documents/data_extracted/Florida/mask.tif');
mask = mask(:,:,1:3);
mask = im2bw(mask,0.5);

gwrite('/application/pi/Downloads/dineof-3.0/Example_CHl/times.dat', dates);
gwrite('/application/pi/Downloads/dineof-3.0/Example_CHl/chl.chl', cube);
gwrite('/application/pi/Downloads/dineof-3.0/Example_CHl/mask.chl.mask', mask);

c = 1;


