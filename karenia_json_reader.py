import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
import os
from PIL import Image
import matplotlib.pyplot as plt
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
import json
import datetime



#-----------------------------------------------------------------
# CODE TO COMPARE SATELLITE DATA WITH INSITU DATA
#-----------------------------------------------------------------

def coord2pixel(x, y, transform_prev):
    """Convert Lat/lon to pixel position"""
    xOrigin = transform_prev[0]
    yOrigin = transform_prev[3]
    pixelWidth = transform_prev[1]
    pixelHeight = -transform_prev[5]
    col = int((x - xOrigin) / pixelWidth)
    row = int((yOrigin -y ) / pixelHeight)
    return(col, row)



def json_reader(file):
    with open(file) as data_file:    
        data = json.load(data_file)

    count_item =  len(data['karenia_brevis_samples'])
    conc_insitu= np.array([])
    conc_sat= np.array([])
    for k in range (0,count_item):
        lat = data['karenia_brevis_samples'][k]['attributes']['latitude']
        lon = data['karenia_brevis_samples'][k]['attributes']['longitude']
        date = data['karenia_brevis_samples'][k]['attributes']['sample_date']
        cell_count = data['karenia_brevis_samples'][k]['attributes']['cellcount']
        category = data['karenia_brevis_samples'][k]['attributes']['category']
        d = str(date)

        ext_date = datetime.datetime.fromtimestamp(int(d[:-3])).strftime('%Y-%m-%d %H:%M:%S')
        d = str(ext_date)
        c = d.split(' ')
        d = c[0].split('-')
        final_date = d[0] + '_' + d[1] + '_' + d[2]       

        file_name = '/application/pi/Documents/data_extracted/Florida2/OLCI/CyanoIndex_' + final_date + '.tif'
        print(final_date + '.tif')

        if os.path.exists(file_name):
            im= Image.open(file_name)
            img_cyano = np.array(im)
            im_raster = gdal.Open(file_name)
            transform_cyano = im_raster.GetGeoTransform()

            col_prev, row_prev = coord2pixel(lon, lat, transform_cyano)
            try:
                cyano_sat_conc = img_cyano[col_prev,row_prev]
                
 
                conc_insitu = np.append(conc_insitu,cell_count)
                conc_sat = np.append(conc_sat,cyano_sat_conc)
                
                print(lat, lon, final_date,cell_count, cyano_sat_conc, category)
            except:
                pass


    plt.plot(conc_insitu) # plotting by columns
    plt.show()
    plt.plot(conc_sat) # plotting by columns
    plt.show()

if __name__ == "__main__":

    origin_folder = '/application/pi/Documents/Create_Projected_Images/noaa_karenia_brevis_2017_formatted.json'
    json_reader(origin_folder)
    