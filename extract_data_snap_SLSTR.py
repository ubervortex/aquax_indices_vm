import subprocess
import cStringIO,io
import os
import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
from envi_to_np import extract_geotiff
from SST_wind_processing import Wind_processing, SST_processing

#-----------------------------------------------------------------
# CODE TO EXTRACT THE IMAGES OF INTEREST FROM THE RAW DATA  
# CHECK THE CORRECT SNAP GRAPH
#-----------------------------------------------------------------


snap_gpt = "gpt"
graph = "myGraph_florida_SLSTR_L2_restricted.xml"


def folder_creation(file_name, region, sensor):

    print('FOLDER CREATOR')
    tree = ET.parse(file_name + '/xfdumanifest.xml')
    root = tree.getroot()
    
    #cycle to extract the acquisition data
    for attributes in root.iter('metadataSection'):
        attribute_list = attributes.getchildren()
        for attribute in attribute_list:
            attribute_list_meta =  attribute.getchildren()
            for att in attribute_list_meta:
                xmlData= att.getchildren()
                for aaaa in xmlData:
                    for data in aaaa.iter('{http://www.esa.int/safe/sentinel/1.1}acquisitionPeriod'):
                        acquisition_data = data[0].text
    
    acquisition_data =  acquisition_data[0:10]
    acquisition_data =  acquisition_data.replace('-','_')

    f = '/application/pi/Documents/data_extracted/' + region + '/' + sensor
    # # check if the folder exist or not
    if not os.path.exists(f):
    #     #folder not present -> create it
        os.makedirs(f)

    return f, acquisition_data


def organize_file(f,acquisition_data):
    
    print ('ORGANIZER')
    #file_path = '../target.data/'
    file_path = '/application/pi/Documents/Create_Projected_Images/target.data/'

    possible_name = ['sea_surface_temperature.hdr', 'sea_surface_temperature.img','wind_speed.hdr', 'wind_speed.img']
    for root, dirs, files in os.walk(file_path, topdown=False):
        for name in dirs:
            shutil.rmtree(file_path + name)

    for root, dirs, files in os.walk(file_path, topdown=False):
        for name in files:
            if any(name in s for s in possible_name):
                extract_geotiff(file_path,  name, f, acquisition_data)
            else:
                os.remove(file_path + name)
    
    for root, dirs, files in os.walk(file_path, topdown=False):
        for name in files:
            os.remove(file_path + name)



def get_metadata(file_name):
    # Create the command line
    try:
        cmd = [snap_gpt, graph, '-Pfile='+file_name]
        dta = subprocess.check_output(cmd, universal_newlines=True)
    except:
        pass
    
    return 'ok' 


if __name__ == "__main__":
    # How to run the code:
    # python extract_data_snap.py '/application/pi/Documents/Sentinel-download/Florida/S3/SLSTR/' Florida SLSTR


    #Main Code to extract all the index of interest
    origin_folder = sys.argv[1]
    region = sys.argv[2]
    sensor = sys.argv[3]

    print('INITIALIZATION')
    for root, dirs, files in os.walk(origin_folder, topdown=False):
        for name in dirs:
            file_path = origin_folder + name
            ret = get_metadata(file_path)
            if ret == 'ok':
                f,acquisition_data = folder_creation(file_path, region, sensor)
                organize_file(f, acquisition_data)

                # ADD HERE OTHER EVENTUAL CODE FOR OTHER INDECES

                SST_processing(f, acquisition_data)

                # try:
                #     Wind_processing(f, acquisition_data)
                # except:
                #     pass
    
    
                   



