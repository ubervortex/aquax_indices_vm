import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
import os
from PIL import Image
#import matplotlib.pyplot as plt
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
import timestamp
import time 
import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta, date
import subprocess


def Wind_processing(f, aquisition_data):
    #Wind processing
    file_name = f + '/wind_speed_'  + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
    wind = np.array(image)
    inDS = gdal.Open(file_name)
  
      
    cols = inDS.RasterXSize
    rows = inDS.RasterYSize
    # 'Get georeference information'
    geotransform = inDS.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    date_f = aquisition_data.replace('_', '')

    #create name of the destination file
    dest_file = f  +'/Wind_speed_' + date_f + '.tif'
    dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform(geotransform)    
    srs = osr.SpatialReference()            
    srs.ImportFromEPSG(4326)                
    dst_ds.SetProjection(srs.ExportToWkt()) 
    dst_ds.GetRasterBand(1).WriteArray(wind)   
    dst_ds.FlushCache()                     
    dst_ds = None
    os.remove(file_name)

def sum_nan_arrays(a,b):
    a[a==0] = np.NaN
    b[b==0] = np.NaN

    fa = np.isfinite(a)
    fb = np.isfinite(b)
    ma = np.isnan(a)
    mb = np.isnan(b)
    ave = np.zeros_like(a)

    ave = np.where(fa&fb, np.where(fa&fb,(a+b)/2,0), ave)
    ave = np.where(fa&mb, np.where(fa&mb,a,ave), ave)
    ave = np.where(ma&fb, np.where(ma&fb,b,ave), ave)
    return ave

def create_vrt_SST(file_1, file_2):

    file_txt = open("image_name.txt", "w")
    file_txt.write(file_1 +"\n" )
    file_txt.write(file_2 +"\n" )
    file_txt.close()
    subprocess.call('gdalbuildvrt -separate -input_file_list image_name.txt output_file.vrt', shell=True)
    dataset = gdal.Open ( "output_file.vrt" )
    data = dataset.ReadAsArray()
    arr = sum_nan_arrays(data[0],data[1])

    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    # 'Get georeference information'
    geotransform = dataset.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    #create name of the destination file
    dest_file = file_1
    dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform(geotransform)    # specify coords
    srs = osr.SpatialReference()            # establish encoding
    srs.ImportFromEPSG(4326)                # WGS84 lat/long
    dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
    dst_ds.GetRasterBand(1).WriteArray(arr)   # write r-band to the raster
    dst_ds.FlushCache()                     # write to disk
    dst_ds = None

    return arr


def SST_processing(f, aquisition_data):
    #SST processing
    file_name = f + '/sea_surface_temperature_'  + aquisition_data + '.tif'
    if os.path.exists(file_name):
        image= Image.open(file_name)
        temperature = np.array(image)
        SST = temperature - 273.15


        val = np.nanmean(SST*2)
        print('------------------------------')
        print(val)

        SST[SST*2 < (val -5)] ='nan'
        SST[SST*2 > (val +5)] ='nan' 

        inDS = gdal.Open(file_name)
        
        cols = inDS.RasterXSize
        rows = inDS.RasterYSize

        # 'Get georeference information'
        geotransform = inDS.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]

        #create name of the destination file
        date_f = aquisition_data.replace('_', '')
        dest_file = f  +'/SST_' + date_f + '.tif'
        if os.path.exists(dest_file):

            print('MERGING')
            dest_file_2 = f  +'/SST_' + date_f + '_2.tif'
            print(dest_file, dest_file_2)
            dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file_2, cols, rows, 1, gdal.GDT_Float32)
            dst_ds.SetGeoTransform(geotransform)    
            srs = osr.SpatialReference()            
            srs.ImportFromEPSG(4326)                
            dst_ds.SetProjection(srs.ExportToWkt()) 
            dst_ds.GetRasterBand(1).WriteArray(SST)   
            dst_ds.FlushCache()                     
            dst_ds = None
            #subprocess.call(['gdal_merge.py','-n', 'nan', '-o' , 'outout.tif', dest_file, dest_file_2])
            create_vrt_SST(dest_file, dest_file_2)
            os.remove(dest_file_2)
            os.remove(file_name)
        else:
            dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
            dst_ds.SetGeoTransform(geotransform)    
            srs = osr.SpatialReference()            
            srs.ImportFromEPSG(4326)                
            dst_ds.SetProjection(srs.ExportToWkt()) 
            dst_ds.GetRasterBand(1).WriteArray(SST)   
            dst_ds.FlushCache()                     
            dst_ds = None
            os.remove(file_name)