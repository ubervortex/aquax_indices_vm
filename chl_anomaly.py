import numpy as np
import io
import json
import shutil
import xml.etree.ElementTree as ET
import sys
import os
from PIL import Image
#import matplotlib.pyplot as plt
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
import time 
import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta, date
import subprocess
from scipy.signal import medfilt


def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


def sum_nan_arrays(a,b):
    # ma = np.isnan(a)
    # mb = np.isnan(b)
    # ave = np.where(ma&mb, np.nan, np.where(ma,0,a) + np.where(mb,0,b))
    m = np.array([a, b])
    ave = np.nanmean(m, axis=0)
    return ave


def diff_nan_arrays(a,b):
    ma = np.isnan(a)
    mb = np.isnan(b)
    ave = np.where(ma&mb, np.nan, np.where(ma,0,a) - np.where(mb,0,b))
    return ave

def create_dataset(f, actual_date, previous_date_m):

    print('----------------------------------')

    file_txt = open("image_name.txt", "w")
    count = 0
    for single_date in daterange(previous_date_m, actual_date):
        #extract the base image
        str_data = single_date.strftime("%Y%m%d")
        print(str_data)
        
        file_name = f  +'projected_CHL_din_' + str_data + '.tif'
        print(file_name)
        if os.path.isfile(file_name):
            count = count +1
            name_f = file_name
            print(name_f)
            file_txt.write(name_f + "\n" )
        # file_name = f  +'CHL_OC4ME_' + str_data + '.tif'
        # if os.path.isfile(file_name):
        #     name_f = file_name
        #     file_txt.write(name_f + "\n" )
        #     count = count +1
            
    file_txt.close()
    print(count)
    if count >1:   
        #'subprocess.call('gdalbuildvrt -separate -input_file_list image_name.txt output_file.vrt', shell=True)
        # command=["gdalbuildvrt", '-te', '-83.41783886554106', '25.696157898050977', '-80.971652423344','28.317071943262114', '-input_file_list', 'image_name.txt', output_file.vrt]    

        command = ['gdalbuildvrt', '-separate', '-input_file_list', 'image_name.txt', 'output_file.vrt']
        subprocess.call(command)
        

        
        dataset = gdal.Open ( "output_file.vrt" )
        data = dataset.ReadAsArray()
        print(len(data))

        mean_im = sum_nan_arrays(data[0],data[1])

        for i in range(2, len(data)-2):
            mean_im = sum_nan_arrays(mean_im,data[i])
        

        mean_im = medfilt(mean_im)

       # plt.imshow(mean_im)
        # plt.show()
        # resulting_image = diff_nan_arrays(mean_im,data[len(data)-1])


        resulting_image = data[len(data)-1] - mean_im
        resulting_image[resulting_image==0] = np.nan
        mean_im[mean_im==0] = np.nan

        # plt.imshow(resulting_image)
        # plt.show()

    
        cols = dataset.RasterXSize
        rows = dataset.RasterYSize
        # 'Get georeference information'
        geotransform = dataset.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]

        str_data = actual_date.strftime("%Y%m%d")
        #create name of the destination file
        dest_file = '/application/pi/Documents/data_extracted/demo_clients/CHL_anomaly2/CHL_anomaly_' + str_data + '.tif'
        dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
        dst_ds.SetGeoTransform(geotransform)    # specify coords
        srs = osr.SpatialReference()            # establish encoding
        srs.ImportFromEPSG(4326)                # WGS84 lat/long
        dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
        dst_ds.GetRasterBand(1).WriteArray(resulting_image)   # write r-band to the raster
        dst_ds.FlushCache()                     # write to disk
        dst_ds = None


def CHL_anomaly(f):
    #Computation of algal bloom anomalies

    for root, dirs, files in os.walk(f, topdown=False):
        for name in files:
            
            date = ""
            if 'CHL' in name:
                date = name.replace('projected_CHL_din_', '')
                date = date.replace('.tif', '')
            if 'CHL_OC4' in name:
                date =  name.replace('CHL_OC4ME_', '')
                date = date.replace('.tif', '')
            if date:
                timestamp= datetime.strptime(date,  "%Y%m%d")
                two_months_earlier = timestamp - relativedelta(days=15)
                create_dataset(f, timestamp, two_months_earlier)

            
if __name__ == "__main__":
    # How to run the code:
    # python extract_data_
    f = '/application/pi/Documents/data_extracted/demo_clients/CHL/'
    CHL_anomaly(f)