# import matlab.engine
from sentinelsat.sentinel import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date, datetime, timedelta
import os.path
import shutil
import sys
import time
import zipfile
import subprocess
from osgeo import gdal, gdalconst 
from osgeo.gdalconst import * 
from osgeo import gdal
from osgeo import osr
from PIL import Image
import numpy as np
from scipy.signal import medfilt
  

from extract_data_snap_OLCI import extract_aquisition_date, get_metadata, organize_file, folder_creation
from index_computation import cyanobacteria_index, CHL_index
from area_cropping import area_cropping

florida_graph = "POLYGON ((-83.5439453125 28.47730827331543, -81.79708862304688 28.47730827331543, -81.79708862304688 26.103370666503906, -83.5439453125 26.103370666503906, -83.5439453125 28.47730827331543, -83.5439453125 28.47730827331543))"
chile_graph = "POLYGON ((-75.90202331542969 -46.46058654785156, -74.05632781982422 -46.46058654785156, -74.05632781982422 -48.1040153503418, -75.90202331542969 -48.1040153503418, -75.90202331542969 -46.46058654785156, -75.90202331542969 -46.46058654785156))"
 

class site_struct():
    def __init__(self, folder, region, sensor, region_polygon):
        self.s_folder = folder
        self.s_region = region
        self.s_sensor = sensor
        self.s_region_polygon = region_polygon

def store_current(f_path,data, acquisition_data, geotransform, param, cols, rows):
    #save images
    last_data = len(data)-1
    data[last_data][np.where((data[last_data] ==0))] = float('NaN')
    # data[last_data] = np.power(10,data[last_data]) #check this step according to all the indices computation

    #create name of the destination file
    dest_file = f_path  + param + '/' + param + '_' + str(acquisition_data) + '.tif'
    dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform(geotransform)    # specify coords
    srs = osr.SpatialReference()            # establish encoding
    srs.ImportFromEPSG(4326)                # WGS84 lat/long
    dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
    dst_ds.GetRasterBand(1).WriteArray(data[last_data])   # write r-band to the raster
    dst_ds.FlushCache()                     # write to disk
    dst_ds = None

def store_history(f_path,data, dates, geotransform, param, cols, rows):
    #save images
    for i in range(0, len(data)):
        data[i][np.where((data[i] ==0))] = float('NaN')
        data[i] = np.power(10,data[i])

        if not os.path.exists(f_path  + param):
            os.makedirs(f_path  + param)

        #create name of the destination file
        dest_file = f_path  + param + '/' + param + '_' + str(dates[i]) + '.tif'
        dst_ds = gdal.GetDriverByName('GTiff').Create(dest_file, cols, rows, 1, gdal.GDT_Float32)
        dst_ds.SetGeoTransform(geotransform)    # specify coords
        srs = osr.SpatialReference()            # establish encoding
        srs.ImportFromEPSG(4326)                # WGS84 lat/long
        dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
        dst_ds.GetRasterBand(1).WriteArray(data[i])   # write r-band to the raster
        dst_ds.FlushCache()                     # write to disk
        dst_ds = None

def run_processing_history(s_info, file_name, history):

    for root, dirs, files in os.walk(s_info.s_folder, topdown=False):
        for name in dirs:
            file_path = s_info.s_folder + name
            ret, data_level = get_metadata(file_path, s_info.s_region_polygon)
            if ret == 'ok':
                f_path,acquisition_data = folder_creation(file_path, s_info.s_region, s_info.s_sensor)
                organize_file(f_path, acquisition_data)
    
    if data_level == 'L1':
        CHL_index(f_path, acquisition_data)
        params = ['CHL'] 
    
    if data_level == 'L2':
        params = ['CHL_OC4', 'ADG443_NN_', 'TSM_NN_']
          
    for p in params:
        file_txt = open("image_name.txt", "w")
        dates =  []
        for root, dirs, files in os.walk(f_path, topdown=False):
            for name in files:     
                date = ""
                if p in name:
                    date =  name.replace(p, '')
                    date = date.replace('.tif', '')
                if date:
                    # timestamp= datetime.strptime(date,  "%Y_%m_%d")
                    dates.append(date)
                    file_txt.write(f_path + name + "\n" )

        file_txt.close()

        #create the cube of overlapping images
        subprocess.call('gdalbuildvrt -separate -input_file_list image_name.txt output_file.vrt', shell=True)
        dataset = gdal.Open ( "output_file.vrt" )
        data = dataset.ReadAsArray()
        cols = dataset.RasterXSize
        rows = dataset.RasterYSize
        # 'Get georeference information'
        geotransform = dataset.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]
            
        store_history(f_path,data, dates, geotransform, p, cols, rows)
        
    
    overallfiles = os.listdir(f_path)
    for ff in overallfiles:
        if '.tif' in ff:
            os.remove(f_path + ff)            

    return f_path

def run_processing(s_info, file_name, history):

    ret, data_level = get_metadata(s_info.s_folder + file_name, s_info.s_region_polygon)
    if ret == 'ok':
        f_path,acquisition_data = folder_creation(s_info.s_folder + file_name, s_info.s_region, s_info.s_sensor)
        organize_file(f_path, acquisition_data)
        
        if data_level == 'L1':
            CHL_index(f_path, acquisition_data)
            params = ['CHL'] 
        
        if data_level == 'L2':
            params = ['CHL_OC4', 'ADG443_NN_', 'TSM_NN_']

        for p in params:
            file_txt = open("image_name.txt", "w")
            dates =  []
            for root, dirs, files in os.walk(s_info.s_folder, topdown=False):
                for name in files:     
                    date = ""
                    if p in name:
                        date =  name.replace(p, '')
                        date = date.replace('.tif', '')
                    if date:
                        # timestamp= datetime.strptime(date,  "%Y_%m_%d")
                        dates.append(date)
                        file_txt.write(f + name + "\n" )

            file_txt.close()

            #create the cube of overlapping images
            subprocess.call('gdalbuildvrt -separate -input_file_list image_name.txt output_file.vrt', shell=True)
            dataset = gdal.Open ( "output_file.vrt" )
            data = dataset.ReadAsArray()
            cols = dataset.RasterXSize
            rows = dataset.RasterYSize
            # 'Get georeference information'
            geotransform = dataset.GetGeoTransform()
            originX = geotransform[0]
            originY = geotransform[3]
            pixelWidth = geotransform[1]
            pixelHeight = geotransform[5]

            store_current(f_path,data, acquisition_data, geotransform, p, cols, rows)
            
    return f_path


def check_download_images(s_info, history, past):

    # connect to the API
    api = SentinelAPI('moranduzzo', 'Y801Jthl',  'https://coda.eumetsat.int')

    # search by polygon, time, and Hub query keywords
    # footprint = geojson_to_wkt(read_geojson(s_info.s_area))
    footprint = s_info.s_region_polygon
    
    current_folder = os.getcwd() # current folder

    print('new test')
    today_date = datetime.today()
    yesterday_date = datetime.today() - timedelta(days=past) # how far we want to go back in the history

    products = api.query(footprint, date = (yesterday_date, today_date), platformname = 'Sentinel-3', filename = 'S3A_OL_2_WFR*')
    print('number of new detected products: ', len(products))
    if len(products)>1:
        #convert OrderedDict to List
        items = list(products.items())
        for i in items:
            file_name = i[1]['filename']
            file_identifier = i[1]['identifier']
            file_id = i[0]
            file_path = s_info.s_folder + '/'+  file_name

            if not os.path.exists(file_path):
                print(file_id)
                api.download(file_id)
                   
                #unzip the file
            zip_name = current_folder + '/' + file_identifier + '.zip'
            with zipfile.ZipFile(zip_name,"r") as zip_ref:
                zip_ref.extractall(current_folder + '/')

            #remove the original file
            os.remove(zip_name) 
            time.sleep(2)

            #move the file in the correct folder
            shutil.move(current_folder + '/' + file_name, s_info.s_folder)    

            #run the processing to extract the index
            if history == 'no':
                f_path = run_processing(s_info, file_name, history)
            
    #run the processing to extract the index
    if history == 'yes':
        f_path = run_processing_history(s_info, file_name, history)

        
        # params = ['CHL_OC4', 'ADG443_NN_', 'TSM_NN_']
        # for p in params:
        #     #call dineof steps
            # eng = matlab.engine.start_matlab()
            # #preparing dineof
            # c = eng.processing_dineof_data(f_path + p)

            # #run dineof
            # os.system('sh /application/pi/Downloads/dineof-3.0/Example_CHl/running.sh')

            # #extract dineof
            # desf_folder = f_path + p + '/' + p
            # if not os.path.exists(desf_folder):
            #     #     #folder not present -> create it
            #     os.makedirs(desf_folder)
            # e = eng.extracting_dineof_data(f_path + p, f_path + p + '/' + p)




if __name__ == "__main__":

    # How to run the code:
    #python extract_data_snap.py Florida OLCI
    #simply pass the folder that contains the images of that specific area

    #define site_struct(self, folder, area, region, sensor)
    folder = '/application/pi/Documents/Sentinel-download/' + sys.argv[1] + '/S3/' +  sys.argv[2] + '/'

    #check which graph apply to the specific region
    if sys.argv[1]=="Florida":
        region_polygon = florida_graph
    if sys.argv[1]=="Chile3":
        region_polygon = chile_graph

    #creation of the site structure
    s_info = site_struct(folder, sys.argv[1], sys.argv[2], region_polygon)

    #check if the folder already exist or not
    if not os.path.exists(folder):   
        #create the folder and run the history download and processing steps
        os.makedirs(folder)
        check_download_images(s_info, 'yes', 30) # check the past 30 days, download and process the history

    while True:
        #start listening for a new image
        check_download_images(s_info, 'no', 2) # check the past 2 days, download and process jsut the last data
        time.sleep(7200)


